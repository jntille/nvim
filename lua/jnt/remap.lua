
-- set space as <leader> key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Keymaps for better default experience
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- open netrw
vim.keymap.set("n", "<leader>pv", ":Ex<CR>", {silent = true }) 


-- center screen in the middle after page up/down
vim.keymap.set("n", "<C-d>", "<C-d>zz", {silent = true })
vim.keymap.set("n", "<C-u>", "<C-u>zz", {silent = true })


vim.keymap.set("n", "n", "nzzzv", {silent = true })
vim.keymap.set("n", "N", "Nzzzv", {silent = true })


-- This is going to get me cancelled
vim.keymap.set("i", "<C-c>", "<Esc>", {silent = true })


-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)