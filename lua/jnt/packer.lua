-- This file can be loaded by calling `lua require("plugins")` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd([[packadd packer.nvim]])

return require("packer").startup(function(use)
  -- Packer can manage itself
  use("wbthomason/packer.nvim")

  -- add your packages here

  -- harpoon
  use("nvim-lua/plenary.nvim") -- required by harpoon, telescope
  use("theprimeagen/harpoon")

  -- Configurations for Nvim LSP
  use { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    requires = {
      -- Useful status updates for LSP
      'j-hui/fidget.nvim',
    },
  }

  -- treesitter for incremental parsing
  -- parsers are installed with :TSInstall
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use { -- Additional text objects via treesitter
  'nvim-treesitter/nvim-treesitter-textobjects',
  after = 'nvim-treesitter',
  }


  -- auto complete
  use("hrsh7th/nvim-cmp")
  use("hrsh7th/cmp-nvim-lsp")
  use("hrsh7th/cmp-buffer")
  use("hrsh7th/cmp-path")
  use("hrsh7th/cmp-cmdline")

  -- "gc" to comment visual regions/lines
  use 'numToStr/Comment.nvim' 

  -- telescope
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable 'make' == 1 }

  -- Git related
  use 'tpope/vim-fugitive'
  use 'tpope/vim-rhubarb'
  use 'lewis6991/gitsigns.nvim'
  

  -- color scheme
  use("folke/tokyonight.nvim")

  -- statusline
  use 'nvim-lualine/lualine.nvim'


end)
